<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme and one of the
 * two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @since DKConduite 0.1
 */

get_header(); ?>  

<section id="slide" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/full-section-img.jpg');">
    <div class="container">
      <div class="row">
        <div class="col-lg-6">
          <div class="img-frame">
            <figure><img src="<?php echo get_template_directory_uri(); ?>/images/bg-slide.png" class="img-fluid" alt="permis"></figure>
          </div>
        </div>
        <div class="col-lg-6">
          <div class="text-slide">
            <h1><?php _e('Louez, apprenez et conduisez', 'dkconduite'); ?></h1>
            <p><?php _e('DKConduite : votre location de voiture à double commande pour un permis B moins cher !', 'dkconduite'); ?></p>
            <a href="<?php echo esc_url( home_url( '/contact/' ) ); ?>" class="btn">Obtenez Votre Permis</a>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section id="section1" class="py-7">
    <div class="container">
      <div class="row">
        <div class="col-12 ">
          <p><?php _e('Comment faire pour louer votre voiture à double commande ? Rien de plus simple avec DK Conduite, 
            votre Bureau de location de voiture à double commande à Strasbourg ! En tant que professionnels, 
            nous assurons une location facile et rapide pour que vous puissiez enfin prendre la route et vous 
            entraînez à passer le permis de conduire plus rapidement et sereinement.', 'dkconduite'); ?>
          </p>
        </div>
      </div>
    </div>
  </section>
<section id="section2" class="py-7">
    <div class="container">
      <div class="row">
        <div class="col-lg-6">
          <img src="<?php echo get_template_directory_uri(); ?>/images/DKConduite-1.JPG" class="img-fluid" alt="DKConduite">
        </div>
        <div class="col-lg-6">
          <h2 class="section-title"><?php _e('Notre service : la location de voiture à double commande', 'dkconduite'); ?></h2>
          <p><?php _e('DKConduite propose son nouveau service de location de voiture à double commande pour faciliter 
            l’apprentissage du permis B aux jeunes conducteurs. Que vous soyez anxieux ou que les horaires 
            de l’auto-école ne vous conviennent pas, c’est une solution pratique et surtout économique.', 'dkconduite'); ?>
          </p>
          <h4 class="sub-title"><?php _e('Une solution pour tous les apprentis conducteurs', 'dkconduite'); ?></h4>
          <p>
          <?php _e('Apprenez à nos côtés comment faire pour louer votre voiture à double commande et parcourez toutes 
            les routes avec un accompagnant de confiance. Nous proposons une offre destinée aux jeunes, aux 
            étudiants et plus largement à toute personne qui souhaite effectuer des économies lorsqu’elle 
            passe son permis de conduire. Notre service est une réponse aux heures de conduite dispensées en 
            auto-école, dont le prix est un frein pour de nombreux candidats à l’examen.', 'dkconduite'); ?>
          </p>
          <p>
          <?php _e('Avec son service de location de voiture à double commande, DK Conduite vous permet de passer votre 
            permis de conduire à moindre coût et de manière personnalisée. Notre service vous permet de gagner 
            du temps comme de vous préparer à l’examen car la voiture est de type d’auto-école.', 'dkconduite'); ?>
          </p>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-6">
          <h4 class="sub-title"><?php _e('Trouvez votre accompagnateur !', 'dkconduite'); ?></h4>
          <p>
          <?php _e('Pour trouver votre accompagnateur, sachez que rien n’est plus facile. En effet, vous avez comme possibilité : 
            Trouvez une personne de confiance qui détient le permis depuis plus de 5 ans Vous n’êtes donc pas tenu de choisir 
            une personne qui est moniteur d’auto-école de profession. Ces solutions vous permettent de choisir un proche digne 
            de confiance, avec qui vous pourrez vous entraîner et qui vous donnera les bons conseils lors de votre parcours sur 
            la route. Si besoin est, notre bureau DK Conduite reste à votre entière disposition pour vous fournir tous les meilleurs 
            conseils à vous ainsi qu’à votre accompagnateur pour prendre en main votre location de voiture à double commande. 
            La location de voiture à double commande permet de vous entraîner au pilotage quand vous le souhaitez. Vous pourrez par 
            exemple vous rendre avec votre accompagnateur sur le parcours de l’examen. Grâce à votre meilleure connaissance du parcours, 
            vous augmentez fortement vos chances de réussite.', 'dkconduite'); ?>
          </p>
        </div>
        <div class="col-lg-6">
          <h2 class="section-title"><?php _e('Une solution plus économique et pratique pour tous', 'dkconduite'); ?></h2>
          <p>
          <?php _e('Marre de payer des sommes importantes en auto-école pour prendre des heures de conduite supplémentaires ? Optez pour la solution 
            DK Conduite et perfectionnez votre conduite avec une location de voiture à double commande.', 'dkconduite'); ?>
          </p>
          <h4 class="sub-title"><?php _e('Réalisez des économies sur le court terme', 'dkconduite'); ?></h4>
          <p>
          <?php _e('Plutôt que de vous demander comment faire pour louer votre voiture à double commande, et finalement préférer vous dirigez vers une 
            auto-école, adoptez la solution DK Conduite en bénéficiant de notre formule défiante toute concurrence.', 'dkconduite'); ?>
          </p>
        </div> 
      </div>
    </div>
  </section>

  <section id="banner" class="banner py-7" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/DK-banner.jpg');">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="call-to-action text-center">
            <div class="area-title">
              <h6><?php _e('// DKConduite : Nos voitures à double pédale //', 'dkconduite'); ?></h6>
              <h2><?php _e('Pour apprendre à votre rythme, sans stress ni pression, optez pour la location de voiture à double commande avec DKConduite.', 'dkconduite'); ?></h2>
            </div>
            <div class="btn-wrapper">
              <a href="<?php echo esc_url( home_url( '/contact/' ) ); ?>" class="btn-wrap-2">Nous Contacter</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="dk-to-left"><img src="<?php echo get_template_directory_uri(); ?>/images/DKConduite-4-1.png" class="img-fluid" alt=""></div>
    <div class="dk-to-right"><img src="<?php echo get_template_directory_uri(); ?>/images/Dkuser.png" class="img-fluid" alt=""></div>
  </section>

  <section id="section3" class="section3 py-7">
    <div class="container">
      <h2 class="section-title"><?php _e('Ne patientez plus des heures avant votre tour'); ?></h2>
        <p><?php _e('De nombreuses auto-écoles sont parfois débordées face à la demande, pour un nombre de moniteurs restreint. Afin de ne pas patienter des heures 
        dans des files d’attente pour placer vos heures de conduite du mois, la location de voiture à double commande vous permet de reprendre le contrôle 
        sur vos heures.', 'dkconduite'); ?>
        </p>
      <div class="row justify-content-center mt-5">
        <div class="col-lg-4 col-sm-6 col-12 mb-2">
          <div class="feature-1 text-center h-100">
            <div class="icon"><i class="fas fa-car-alt fa-4x"></i></div>
            <div class="info">
              <h3><?php _e('Prenez le contrôle de vos heures de conduite', 'dkconduite'); ?></h3>
              <p><?php _e('Vous pouvez décider de ne pas conduire ce jour si vous n’en avez pas envie, comme d’improviser un cours quand vous avez dégagé du temps libre 
                et que votre accompagnant est disponible.', 'dkconduite'); ?>
              </p>
              <a href="<?php echo esc_url( home_url( '/contact/' ) ); ?>">En Savoir Plus</a>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-sm-6 col-12 mb-2">
          <div class="feature-2 text-center h-100">
            <div class="icon"><i class="fas fa-walking fa-4x"></i></div>
            <div class="info">
              <h3><?php _e('DKConduite : pourquoi attendre ?', 'dkconduite'); ?></h3>
              <p><?php _e('Abordez votre examen en toute sérénité grâce à l’expérience acquise pendant vos heures d’apprentissage supplémentaires. Notre service est la 
                solution alternative aux tarifs des auto-écoles traditionnelles.', 'dkconduite'); ?> 
              </p>
              <a href="<?php echo esc_url( home_url( '/contact/' ) ); ?>">En Savoir Plus</a> 
            </div>
          </div>
        </div> 
        <div class="col-lg-4 col-sm-6 col-12 mb-2">
          <div class="feature-1 text-center h-100">
            <div class="icon"><i class="fas fa-car-side fa-4x"></i></div>
            <div class="info">
              <h3><?php _e('Passez à la vitesse supérieure', 'dkconduite'); ?></h3>
              <p><?php _e('Une multitude d’apprentis, de candidats libres et de personnes devant repasser leur permis ont déjà opté pour nos services. Vous aussi, 
                passez à la vitesse supérieure !', 'dkconduite'); ?>
              </p>
              <a href="<?php echo esc_url( home_url( '/contact/' ) ); ?>">En Savoir Plus</a>
            </div>
          </div>
        </div> 
      </div>
    </div>
  </section>

<?php get_footer(); ?>