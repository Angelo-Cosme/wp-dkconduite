<?php
/**
 *
 * @package WordPress
 * @since DKConduite 0.1
 */

get_header(); ?>

  <?php /* The loop */ ?>
  <?php while ( have_posts() ) :
    the_post(); ?>

  <section id="profile" class="profile-numerica profile-<?php the_ID(); ?> py-11">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-lg-11 col-xl-10 col-xxl-9 mx-auto py-7">
          <div class="row">
            <?php if ( has_post_thumbnail() ) : ?>
            <div class="col-md-4 col-lg-6 order-md-2">
              <?php the_post_thumbnail('member', array('class="img-fluid"')); ?>
            </div>
            <?php endif; ?>
            <div class="col-md-8 col-lg-6 order-md-1">
              <h2><?php the_tite(); ?></h2>
              <?php the_content(); ?>

              <ul class="social">
                <li><a href="#" class="facebook" data-abc="true"><i class="fab fa-facebook"></i></a></li>
                <li><a href="#" class="google-plus" data-abc="true"><i class="fab fa-linkedin"></i></a></li>
                <li><a href="#" class="google-plus" data-abc="true"><i class="fab fa-twitter"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <?php endwhile; ?>

<?php get_footer(); ?>