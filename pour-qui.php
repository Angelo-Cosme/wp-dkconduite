<?php
/**
 * Template name: Pour-Qui
 *
 * @package WordPress
 * @since SOAGA 0.1
 */

get_header(); ?>
  
  <section id="slide" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/for-what.jpg');">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="text-slide w-50">
            <h1><?php _e('Une location de voiture à double commande ?', 'dkconduite'); ?></h1>
            <p><?php _e('Qui sont concerner ? Vous souhaitez réaliser des économies sur le coût que représente votre apprentissage du volant ?', 'dkconduite'); ?></p>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section id="for-who" class="for-who">
    <div class="container">
      <div class="row">
        <div class="col-lg-4 col-sm-6 mb-2">
          <div class="single-box h-100">
            <div class="icon"><img src="<?php echo get_template_directory_uri(); ?>/images/single-box.png" class="img-fluid" alt=""></div>
            <h3><?php _e('Réalisez des économies', 'dkconduite'); ?></h3>
            <p><?php _e('La solution que nous vous proposons est tout à fait intéressante, puisqu’elle vous permettra de limiter le coût des heures 
              supplémentaires en auto-école. Du lundi au dimanche, de 8h à 21h sans interruption, bénéficier de tarifs dégressifs. Condition à 
              remplir pour la location de véhicule à double commande.', 'dkconduite'); ?>
            </p>
          </div>
        </div>
        <div class="col-lg-4 col-sm-6 mb-2">
          <div class="single-box h-100">
            <div class="icon"><img src="<?php echo get_template_directory_uri(); ?>/images/single-box.png" class="img-fluid" alt=""></div>
            <h3><?php _e('Obligations d’accompagnement', 'dkconduite'); ?></h3>
            <p><?php _e('Tout le monde peut devenir votre accompagnateur et prétendre à une location de voiture à double pédale, à condition de remplir un 
              certain nombre de critères. Il doit être âgé de plus de 25 ans et détenir son permis depuis au moins 5 ans. ', 'dkconduite'); ?>
            </p>
          </div>
        </div>
        <div class="col-lg-4 col-sm-6 offset-sm-3 offset-lg-0 mb-2">
          <div class="single-box h-100">
            <div class="icon"><img src="<?php echo get_template_directory_uri(); ?>/images/single-box.png" class="img-fluid" alt=""></div>
            <h3><?php _e('Qui peut en bénéficier ?', 'dkconduite'); ?></h3>
            <p><?php _e('Apprentis conducteurs : dès lors que vous comptez au moins 20h de conduite en compagnie d’un moniteur. ACC -16 : vous devez avoir au moins 16 
              ans et avoir validé l’Apprentissage Anticipé de la Conduite. Candidats libres et une Remise à niveau accompagné de la personne de votre choix.', 'dkconduite'); ?>
            </p>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section id="abt" class="container py-11">
      <div class="row align-items-center">
        <div class="col-lg-6">
          <div class="abt-img">
            <img src="<?php echo get_template_directory_uri(); ?>/images/DKconduite-4-1.png" class="img-fluid" alt="">
            <div class="abt-shape"><img src="<?php echo get_template_directory_uri(); ?>/images/about-3-shape.png" alt=""></div>
            <div class="experience">
              <img src="<?php echo get_template_directory_uri(); ?>/images/experience-shape.png" class="img-fluid" alt="">
              <div class="experience-two">
                <div class="content">
                  <h3><?php _e('+10', 'dkconduite'); ?></h3>
                  <p><?php _e('années d\'expériences', 'dkconduite'); ?></p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-6">
          <div class="abt-content">
            <div class="row">
              <div class="col-lg-6 col-sm-6">
                <div class="abt-permis">
                  <h3><?php _e('EN AUTO-ÉCOLE', 'dkconduite'); ?></h3>
                  <p><?php _e('Vous suivez des cours en auto-école et vous avez effectué plus de 20h de conduite avec votre 
                    moniteur ? La location de voiture double commande est faite pour vous !', 'dkconduite'); ?></p>
                </div>
              </div>
              <div class="col-lg-6 col-sm-6">
                <div class="abt-permis">
                  <h3><?php _e('PERMIS ACC – 16', 'dkconduite'); ?></h3>
                  <p><?php _e('Vous avez passé votre permis ACC-16 et vous voulez valider vos compétences face à votre moniteur ? 
                    La location de voiture double commande est faite pour vous !', 'dkconduite'); ?></p>
                </div>
              </div>
              <div class="col-lg-6 col-sm-6">
                <div class="abt-permis">
                  <h3><?php _e('CANDIDAT LIBRE', 'dkconduite'); ?></h3>
                  <p><?php _e('Vous vous présentez au permis de conduire en candidat libre et vous avez besoin d’un véhicule homologué ? 
                    La location de voiture double commande est faite pour vous !', 'dkconduite'); ?></p>
                </div>
              </div>
              <div class="col-lg-6 col-sm-6">
                <div class="abt-permis">
                  <h3><?php _e('REMISE À NIVEAU', 'dkconduite'); ?></h3>
                  <p><?php _e('Vous avez votre permis, mais vous n’avez pas conduit depuis longtemps ? Une remise à niveau s’impose. 
                    La location de voiture double commande est faite pour vous !', 'dkconduite'); ?></p>
                </div>
              </div>
            </div>
            <a href="<?php echo esc_url( home_url( '/contact/' ) ); ?>" class="btn">En Savoir Plus</a>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section id="section2" class="py-11">
    <div class="container">
      <h2 class="section-title text-center mb-7"><?php _e('Liste des pièces à fournir', 'dkconduite'); ?></h2>
      <div class="row">
        <div class="col-lg-6">
          <h4 class="sub-title"><?php _e('L\'apprenti', 'dkconduite'); ?></h4>
          <ul>
            <li><i class="fas fa-check-circle"></i><?php _e('Le livret d’apprentissage avec minium 20h de conduite à l’auto école.', 'dkconduite'); ?> </li>
            <li><i class="fas fa-check-circle"></i><?php _e('Un justificatif de domicile.', 'dkconduite'); ?> </li>
            <li><i class="fas fa-check-circle"></i><?php _e('Une pièce d’identité / Titre de séjour valide. ', 'dkconduite'); ?> </li>
          </ul>
        </div>
        <div class="col-lg-6">
          <h4 class="sub-title"><?php _e('', 'dkconduite'); ?>L’accompagnateur</h4>
          <ul>
            <li><i class="fas fa-check-circle"></i><?php _e('Une pièce d’ideentité / Titre de séjour valide', 'dkconduite'); ?> </li>
            <li><i class="fas fa-check-circle"></i><?php _e('Un justificatif de domicile.', 'dkconduite'); ?> </li>
            <li><i class="fas fa-check-circle"></i><?php _e('Le permis de conduire. ', 'dkconduite'); ?> </li>
            <li><i class="fas fa-check-circle"></i><?php _e('Un chèque de 300£ pour la caution.', 'dkconduite'); ?> </li>
          </ul>
        </div>
      </div>
    </div>
  </section>
  
<?php get_footer(); ?>