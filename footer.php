<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @since DKConduite 0.1
 */
?>

<section id="footer" class="footer py-11">
  <div class="container">
    <div class="row">
      <div class="col-lg-4">
        <h3>Information</h3>
        <ul>
          <li><a href="<?php echo esc_url( home_url( '/pour-qui/' ) ); ?>">À Propos de nous</a></li>
          <li><a href="<?php echo esc_url( home_url( '/contact/' ) ); ?>">Centre d'aide</a></li>
          <li><a href="<?php echo esc_url( home_url( '/contact/' ) ); ?>">Contact</a></li>
        </ul>
      </div>
      <div class="col-lg-4">
        <h3>Agence Principale</h3>
        <ul>
          <li><a href="<?php echo esc_url( home_url( '/strasbourg/' ) ); ?>">Strasbourg</a></li>
          <p class="mt-3">DKConduite notre Expertise fait parlé de nous depuis plus de 10 ans
            Nous mettons les meilleurs équipements à la disposition de nos apprenants
            afin de leur permettre une réussite totale.
          </p>  
        </ul>
      </div>
      <div class="col-lg-4">
        <h3>Newsletter</h3>
        <div class="input-group">
          <input type="email" class="form-control" placeholder="Votre adresse Email" aria-label="Recipient's username" aria-describedby="basic-addon2">
          <a class="input-group-text btn">Souscrire</a>
        </div>
        <div class="new">
          <p><a href="index.html">DKConduite</a> Incrivez vous à notre newsletter pour rester informer sur nos differentes offres</p>
        </div>
      </div>

      <div class="footer-bottom">
        <ul>
          <li><a href="index.html" class="active">DKConduite</a></li>
          <li><a href="#"><i class="fab fa-facebook-square fa-2x"></i></a></li>
          <li><a href="#"><i class="fab fa-linkedin fa-2x"></i></a></li>
          <li><p>Copyright © 2021 <a href="https://www.facebook.com/FGANumerik/services">FgaNumerik</a>. Tous Droits Réservés |</p></li>
          <li><a href="#">Condition & Politique d'Utilisation </a></li>
         </ul>
      </div>
    </div>
  </div>
</section>
</div><!-- #page -->

	<?php wp_footer(); ?>
</body>
</html>
