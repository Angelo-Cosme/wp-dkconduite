<?php
/**
 * Template name: Manuelle
 *
 * @package WordPress
 * @since SOAGA 0.1
 */

get_header(); ?>
  
  <section id="slide" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/slider_img1.jpg');">
    <div class="container">
      <div class="row">
        <div class="col-lg-6">
          <div class="text-slide">
            <h1><?php _e('Nos prix pour les Boites Manuelles', 'dkconduite'); ?></h1>
            <p><?php _e('Profitez de tarifs avantageux sur une location de voiture à double pédale', 'dkconduite'); ?></p>
            <a href="<?php echo esc_url( home_url( '/contact/' ) ); ?>" class="btn rounded-0">Obtenez Votre Permis</a>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section id="section1" class="py-7">
    <div class="container">
      <div class="row">
        <div class="col-12 ">
          <p><?php _e('En plus d’être une solution économique par rapport aux heures de conduite dispensées dans 
            une auto-école traditionnelle, Permis UP vous propose des tarifs dégressifs pour votre location 
            de voiture à double pédale. Plus vous réservez d’heures, moins le prix est élevé ! Etonnant, 
            n’est-ce pas pour une location de voiture à double commande ? Le prix du marché dans ce secteur 
            d’activité est de 20€ en moyenne. C’est la politique que nous pratiquons lorsque vous optez pour 
            une heure de conduite à l’unité.', 'dkconduite'); ?>
          </p>
        </div>
      </div>
    </div>
  </section>

  <section id="section2" class="py-7">
    <div class="container">
      <div class="row">
        <div class="col-lg-6">
          <img src="<?php echo get_template_directory_uri(); ?>/images/DKconduite-3-1.png" class="img-fluid" alt="DKconduite">
        </div>
        <div class="col-lg-6">
          <p><?php _e('Avec notre système de prix dégressif, vous pouvez diminuer de moitié ce prix unitaire. En choisissant 
            une formule à long terme qui comprend plusieurs heures de conduite : vous pouvez réduire le prix du service 
            jusqu’à 10€ de l’heure.', 'dkconduite'); ?>
          </p>
          <p>
            <?php _e('Une offre plus qu’intéressante pour les jeunes apprentis, inscrits depuis peu au permis de conduire qui ont 
            encore un nombre d’heures important à effectuer avant de pouvoir se présenter à l’examen avec la pratique 
            nécessaire.', 'dkconduite'); ?>
          </p>
          <p>
            <?php _e('La solution intéresse également les conducteurs aguerris qui viennent de se voir retirer leur permis et 
            doivent alors repasser l’examen. Dès réinscription, ils optent pour un forfait de plusieurs heures afin 
            de réduire les coûts et de jouir de la location de voiture à double pédale.', 'dkconduite'); ?>
          </p>
          <p>
            <?php _e('Les candidats libres adoptent également ce comportement pour avoir à leur disposition un véhicule homologué 
            pour se présenter à l’examen. On trouve même des personnes en possession du permis qui souhaitent simplement 
            pratiquer avec une personne plus aguerrie.', 'dkconduite'); ?>
          </p>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-6">
          <p>
           <?php _e('En plus de nos tarifs dégressifs, nous proposons des forfaits franchisés pour plus de tranquillité. Ces formules
            permettent de réduire le montant de la responsabilité financière applicable en cas de dommage causé au véhicule 
            à 200€ contre 500 €.', 'dkconduite'); ?> 
          </p>
        </div>
        <div class="col-lg-6">
          <p>
            <?php _e('Consultez toutes nos offres et n’hésitez pas à demander conseil à nos équipes pour faire le choix optimal. Avec 
            Permis UP redécouvrez le permis B grâce à la location de voiture à double commande.', 'dkconduite'); ?>
          </p>
        </div>
      </div>
    </div>
  </section>

  <section id="price-table" class="py-7">
    <div class="container">
      <div class="row justify-content-center">
        <h2 class="section-title text-center mb-7"><?php _e('NOS FORFAITS CLASSIQUES DE VOITURE A DOUBLE COMMANDE', 'dkconduite'); ?></h2>
        <div class="col-lg-3 col-md-6 mb-3 mx-auto">
          <div class="pricing-plan text-center">
            <div class="pricing-title">
              <h3><?php _e('À L\'UNITÉ 1H', 'dkconduite'); ?></h3>
              <span>1H</span>
            </div>
            <div class="price">
              <span>€</span> 20
            </div>
            <div class="pricing-content clearfix">
              <ul>
                <li><?php _e('Valable 1 mois', 'dkconduitte'); ?></li>
                <li><?php _e('Kilométrage illimité', 'dkconduitte'); ?></li>
                <li><?php _e('Assurance incluse', 'dkconduitte'); ?></li>
                <li><?php _e('Essence incluse', 'dkconduitte'); ?></li>
              </ul>
            </div>
            <div class="price-btn">
              <a href="<?php echo esc_url( home_url( '/contact/' ) ); ?>">Louer</a>
            </div>
          </div>
        </div> 
        <div class="col-lg-3 col-md-6 mb-3 mx-auto">
          <div class="pricing-plan text-center">
            <div class="pricing-title">
              <h3><?php _e('FORFAIT 5H', 'dkconduite'); ?></h3>
              <span>5H (19,80 €/H)</span>
            </div>
            <div class="price">
              <span>€</span>99
            </div>
            <div class="pricing-content clearfix">
              <ul>
                <li><?php _e('Valable 1 mois', 'dkconduitte'); ?></li>
                <li><?php _e('Kilométrage illimité', 'dkconduitte'); ?></li>
                <li><?php _e('Assurance incluse', 'dkconduitte'); ?></li>
                <li><?php _e('Essence incluse', 'dkconduitte'); ?></li>
              </ul>
            </div>
            <div class="price-btn">
              <a href="<?php echo esc_url( home_url( '/contact/' ) ); ?>">Louer</a>
            </div>
          </div>
        </div> 
        <div class="col-lg-3 col-md-6 mb-3 mx-auto">
          <div class="pricing-plan text-center">
            <div class="pricing-title">
              <h3><?php _e('FORFAIT 10H', 'dkconduite'); ?></h3>
              <span>10H (19 €/H)</span>
            </div>
            <div class="price">
              <span>€</span>190
            </div>
            <div class="pricing-content clearfix">
              <ul>
                <li><?php _e('Valable 3 mois', 'dkconduitte'); ?></li>
                <li><?php _e('Kilométrage illimité', 'dkconduitte'); ?></li>
                <li><?php _e('Assurance incluse', 'dkconduitte'); ?></li>
                <li><?php _e('Essence incluse', 'dkconduitte'); ?></li>
              </ul>
            </div>
            <div class="price-btn">
              <a href="<?php echo esc_url( home_url( '/contact/' ) ); ?>">Louer</a>
            </div>
          </div>
        </div> 
        <div class="col-lg-3 col-md-6 mb-3 mx-auto">
          <div class="pricing-plan text-center">
            <div class="pricing-title">
              <h3><?php _e('FORFAIT 20H', 'dkconduite'); ?></h3>
              <span>20H (17 €/H)</span>
            </div>
            <div class="price">
              <span>€</span>340
            </div>
            <div class="pricing-content clearfix">
              <ul>
                <li><?php _e('Valable 6 mois', 'dkconduitte'); ?></li>
                <li><?php _e('Kilométrage illimité', 'dkconduitte'); ?></li>
                <li><?php _e('Assurance incluse', 'dkconduitte'); ?></li>
                <li><?php _e('Essence incluse', 'dkconduitte'); ?></li>
              </ul>
            </div>
            <div class="price-btn">
              <a href="<?php echo esc_url( home_url( '/contact/' ) ); ?>">Louer</a>
            </div>
          </div>
        </div> 
        <div class="col-lg-3 col-md-6 mb-3">
          <div class="pricing-plan text-center">
            <div class="pricing-title">
              <h3><?php _e('FORFAIT JOURNÉE', 'dkconduite'); ?></h3>
              <span>(8H30 À 17H30)</span>
            </div>
            <div class="price">
              <span>€</span>90
            </div>
            <div class="pricing-content clearfix">
              <ul>
                <li><?php _e('Valable la journée', 'dkconduitte'); ?></li>
                <li><?php _e('Kilométrage illimité', 'dkconduitte'); ?></li>
                <li><?php _e('Assurance incluse', 'dkconduitte'); ?></li>
                <li><?php _e('Essence incluse', 'dkconduitte'); ?></li>
              </ul>
            </div>
            <div class="price-btn">
              <a href="<?php echo esc_url( home_url( '/contact/' ) ); ?>">Louer</a>
            </div>
          </div>
        </div> 
        <div class="col-lg-3 col-md-6 mb-3">
          <div class="pricing-plan text-center">
            <div class="pricing-title">
              <h3><?php _e('JOURNÉE EXAMEN', 'dkconduite'); ?></h3>
              <span>(2H)</span>
            </div>
            <div class="price">
              <span>€</span>80
            </div>
            <div class="pricing-content clearfix">
              <ul>
                <li><?php _e('Valable la journée', 'dkconduitte'); ?></li>
                <li><?php _e('Kilométrage illimité', 'dkconduitte'); ?></li>
                <li><?php _e('Assurance incluse', 'dkconduitte'); ?></li>
                <li><?php _e('Essence incluse', 'dkconduitte'); ?></li>
              </ul>
            </div>
            <div class="price-btn">
              <a href="<?php echo esc_url( home_url( '/contact/' ) ); ?>">Louer</a>
            </div>
          </div>
        </div>    
      </div>
    </div>
  </section>

  <section id="banner" class="banner py-7" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/DK-banner.jpg');">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="call-to-action text-center">
            <div class="area-title">
              <h6><?php _e('// Franchise  //', 'dkconduite'); ?></h6>
              <p><?php _e('300 € applicable sur les garanties responsabilité civile et dommages tout accidents responsable. La franchise 
                est donc applicable pour toutes déclaration de sinistre responsable ou sans recours & applicable sur les dommages 
                occasionnés à tiers même en l’abscence des dégats sur le véhicule loué. En cas de vol et incendie la franchise est de 900 €.', 'dkconduite'); ?></p>
            </div>
            <div class="btn-wrapper">
              <a href="<?php echo esc_url( home_url( '/contact/' ) ); ?>" class="btn-wrap-2">Nous Contacter</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="dk-to-left"><img src="<?php echo get_template_directory_uri(); ?>/images/DKconduite-4-1.png" class="img-fluid" alt=""></div>
    <div class="dk-to-right"><img src="<?php echo get_template_directory_uri(); ?>/images/Dkuser.png" class="img-fluid" alt=""></div>
  </section>
  
<?php get_footer(); ?>