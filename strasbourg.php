<?php
/**
 * Template name: Strasbourg
 *
 * @package WordPress
 * @since SOAGA 0.1
 */

get_header(); ?>
  

  <section id="slide" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/full-section-img.jpg');">
    <div class="container">
      <div class="row">
        <div class="col-lg-6">
          <div class="img-frame">
            <figure><img src="<?php echo get_template_directory_uri(); ?>/images/DKconduite-8.png" class="img-fluid" alt="permis"></figure>
          </div>
        </div>
        <div class="col-lg-6">
          <div class="text-slide">
            <h1><?php _e('Notre Agence De Strasbourg', 'dkconduite'); ?></h1>
            <p><?php _e('Notre bureau de location de voiture à double commande à Strasbourg', 'dkconduite'); ?></p>
            <a href="<?php echo esc_url( home_url( '/contact/' ) ); ?>" class="btn">Obtenez Votre Permis</a>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section id="section1" class="py-7">
    <div class="container">
      <div class="row">
        <div class="col-12 ">
          <p><?php _e('En tant que professionnelle de la location de voiture à double commande à 
            Strasbourg, c’est tout naturellement que DK Conduite a ouvert son premier 
            bureau implanté au cœur de la ville de Strasbourg. Notre présence a apporté 
            un immense soulagement au milieu de l’apprentissage de la conduite. Découvrez 
            sans plus attendre nos offres de location de voiture à double commande avec 
            DK Conduite !', 'dkconduite'); ?>
          </p>
        </div>
      </div>
    </div>
  </section>

  <section id="section2" class="py-7">
    <div class="container">
      <div class="row">
        <div class="col-lg-6">
          <img src="<?php echo get_template_directory_uri(); ?>/images/DKconduite.JPG" class="img-fluid" alt="DKconduite">
        </div>
        <div class="col-lg-6">
          <h2 class="section-title"><?php _e('Pourquoi venir dans l’une de nos agences de location de voiture à double commande ?', 'dkconduite'); ?></h2>
          <p><?php _e('Le réseau d’auto-école classique étant fortement bouché dans les grandes villes, l’attente pour les apprentis 
            inscrits à l’examen du permis B est parfois interminable. Ce phénomène est encore plus accentué en Île-de-France.', 'dkconduite'); ?>
          </p>
        </div>
      </div>
      <div class="row mt-7">
        <div class="col-lg-6">
          <h2 class="sub-title"><?php _e('Conduire rapidement pour passer rapidement l’examen du permis de conduire', 'dkconduite'); ?></h2>
          <p> 
            <?php _e('Grâce à la mise en place de multiples agences de location de voiture à double commande DK Conduite 
            partout en Île-de-France, vous gagnez du temps, notamment au niveau de l’attente avant l’examen du permis de conduire.
            Une attente qui, en plus d’avoir un impact sur le moral des élèves impatients de terminer leur cycle de conduite, a 
            également des conséquences sur leurs aptitudes et confiances en eux. Des conséquences qui peuvent se ressentir le jour de l’examen.', 'dkconduite'); ?>
          </p>
          <p>
            <?php _e('Testez une voiture à double pédale dans l’une de nos agences de location de voiture à double commande est une solution 
            pratique pour tous ceux qui souhaitent avoir des heures de conduite régulières et apprendre rapidement à manier un modèle 
            type de véhicule d’auto-école.', 'dkconduite'); ?>
          </p>
        </div>
        <div class="col-lg-6">
          <img src="<?php echo get_template_directory_uri(); ?>/images/DKconduite-3.png" class="img-fluid" alt="DKconduite">
        </div>
      </div>
      <div class="row mt-7">
        <div class="col-lg-6">
          <img src="<?php echo get_template_directory_uri(); ?>/images/DKconduite-4.png" class="img-fluid" alt="DKconduite">
        </div>
        <div class="col-lg-6">
          <h2 class="section-title"><?php _e('Une solution économique pour tous', 'dkconduite'); ?></h2>
          <p><?php _e('En tant qu’apprenti conducteur, et selon votre statut (étudiant, salarié, etc.), rouler à des heures précises peut être difficiles 
            si vous êtes en auto-école. Nous proposons une solution accessible à tous, que vous passiez l’examen pour la première fois ou non. 
            Nous avons pu développer notre réseau pour vous proposer des agences de location de voiture à double commande partout en Île-de-France 
            et dans la capitale.', 'dkconduite'); ?>
          </p>
          <p>
            <?php _e('C’est une solution pratique car vous gagnez du temps pour des heures de conduite placées à votre convenance. Sans oublier que cet investissement 
            est vite rentabilisé. En effet, nous proposons une formule unique à 10€ de l’heure, alors que les heures de conduite en auto-école varient entre 
            50 et 65€ de l’heure.', 'dkconduite'); ?>
          </p>
        </div>
      </div>
      <div class="row mt-7">
        <div class="col-lg-6">
          <h2 class="sub-title"><?php _e('La solution DK Conduite : rejoignez l’une de nos agences de location de voiture à double commande en région parisienne', 'dkconduite'); ?></h2>
          <p><?php _e('Face à l’immobilité des auto-écoles traditionnelles, notre service apparaît comme une véritable bouffée d’air, qui révolutionne l’apprentissage 
            de la conduite. Notre service connecté offre aux utilisateurs d’une voiture à double commande une flexibilité importante qui leur permet d’organiser 
            leur temps d’apprentissage en fonction de leur disponibilité.', 'dkconduite'); ?>
          </p>
        </div>
        <div class="col-lg-6">
          <h2 class="sub-title"><?php _e('Un réseau développé grâce à la confiance de tous les apprentis conducteurs qui nous font confiance', 'dkconduite'); ?></h2>
          <p>
          <?php _e('Notre réseau de professionnels assure aux élèves un apprentissage efficace délivré dans des conditions optimales. De nombreux Franciliens, 
            qu’ils soient jeunes conducteurs, ou initiés devant repasser leur examen, optent avec DK Conduite pour notre service innovant dans nos agences 
            de location de voiture à double commande.', 'dkconduite'); ?>
          </p>
          <p>
          <?php _e('Notre implantation à Paris et en Île-de-France nous permet de vous faire bénéficier d’une couverture optimale. Où que vous soyez, vous trouverez 
            forcément un de nos établissements proches de chez vous pour une location de voiture à double commande. Découvrez la liste ci-dessus, vous trouverez 
            un itinéraire en transports en commun pour vous y rendre le plus rapidement possible.', 'dkconduite'); ?>
          </p>
        </div>
      </div>

      <div class="row mt-7">
        <div class="col-lg-6">
          <h2 class="sub-title"><?php _e('Trouvez votre personne de confiance pour vous enseigner la conduite en toute sécurité', 'dkconduite'); ?></h2>
          <p><?php _e('En venant dans l’une de nos agences de location de voiture à double commande DK Conduite, vous découvrirez que nous avons toujours des solutions. 
            Si vous êtes jeune conducteur, vous pouvez trouver aisément votre accompagnateur.', 'dkconduite'); ?>
          </p>
          <p>
          <?php _e('En effet, vous n’avez plus besoin de vous référer à un moniteur d’auto-école et préférer une personne proche de vous. Si cette personne détient le permis 
            de conduire depuis plus de 5 ans. Ainsi, qu’il s’agisse d’un proche ou d’un ami, vous pouvez organiser votre temps de conduite sur votre temps libre et le sien.', 'dkconduite'); ?>
          </p>
          <p>
          <?php _e('Toutes ces solutions font que trouver votre accompagnateur et coach personnel sont facilitées. Vous gagnez du temps, pour une organisation facilitée avant la date 
            de l’examen final.', 'dkconduite'); ?>
          </p>
        </div>
        <div class="col-lg-6">
          <h2 class="sub-title"><?php _e('Nous sommes présents dans de nombreuses régions Île-de-France', 'dkconduite'); ?></h2>
          <p>
          <?php _e('Découvrez nos agences de location de voiture à double commande DK Conduite à Alfortville (94), Clamart (92), Malakoff (92), Livry-Gargan (93) et à Montgeron (91)! 
            Nous proposons notre formule à 10€ de l’heure dans chacune de nos agences pour votre plus grand plaisir. Pour tous renseignements supplémentaires sur nos agences de 
            location de voiture à double commande, concernant nos services ou pour une éventuelle question, n’hésitez pas à contacter nos équipes par téléphone ou à consulter 
            notre rubrique FAQ !', 'dkconduite'); ?>
          </p>
        </div>
      </div>
    </div> 
  </section>

  <section id="section3" class="section3 py-7">
    <div class="container">
      <h2 class="section-title"><?php _e('Quand venir dans notre agence DKConduite à Strasbourg ?', 'dkconduite'); ?></h2>
        <p><?php _e('Venez nous voir sans plus attendre dans notre agence DKConduite à Strasbourg. Votre location de voiture à double commande n’attend plus que vous.', 'dkconduite'); ?></p>
      <div class="row justify-content-center mt-5">
        <div class="col-lg-4 col-sm-6 col-12 mb-2">
          <div class="feature-1 text-center h-100">
            <div class="icon"><i class="fas fa-clock fa-4x"></i></div>
            <div class="info">
              <h3><?php _e('Horaires', 'dkconduite'); ?></h3>
              <p><?php _e('L’agence DKConduite vous propose la location de voiture à double commande (91) tous les jours de 10h à 19h. Vous pouvez louer nos véhicules de 7h à 22h sans 
                interruption. Les inscriptions se font sur rendez-vous.', 'dkconduite'); ?>
              </p>
              <a href="<?php echo esc_url( home_url( '/contact/' ) ); ?>">En Savoir Plus</a>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-sm-6 col-12 mb-2">
          <div class="feature-2 text-center h-100">
            <div class="icon"><i class="fas fa-map-marked-alt fa-4x"></i></div>
            <div class="info">
              <h3><?php _e('Localisation', 'dkconduite'); ?></h3>
              <p>
                <?php _e('DKConduite Montgeron est située au 12 Rue René Haby, 91230 Montgeron à proximité de Vigneux-sur-seine, Villeneuve-saint-Georges , Brunoy, Yerres, Crosne et Epinay-sous-Sénart', 'dkconduite'); ?>
              </p>
              <a href="<?php echo esc_url( home_url( '/contact/' ) ); ?>">En Savoir Plus</a>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-sm-6 col-12 mb-2">
          <div class="feature-1 text-center h-100">
            <div class="icon"><i class="fas fa-car-side fa-4x"></i></div>
            <div class="info">
              <h3><?php _e('Transports', 'dkconduite'); ?></h3>
              <p><?php _e('Vous avez le choix entre :
                Le RER D : Gare de Montgeron Crosne
                Le bus A : Pont TVM
                Le bus P : Gare de Montgeron', 'dkconduite'); ?>
              </p>
              <a href="<?php echo esc_url( home_url( '/contact/' ) ); ?>">En Savoir Plus</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  
<?php get_footer(); ?>