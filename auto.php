<?php
/**
 * Template name: Automatique
 *
 * @package WordPress
 * @since SOAGA 0.1
 */

get_header(); ?>
  
  <section id="slide" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/slider_img1.jpg');">
    <div class="container">
      <div class="row">
        <div class="col-lg-6">
          <div class="text-slide">
            <h1><?php _e('Nos prix pour les Boites Automatiques', 'dkconduite'); ?></h1>
            <p><?php _e('Louez une voiture à double commande à boîte automatique chez DK conduite!', 'dkconduite'); ?></p>
            <a href="<?php echo esc_url( home_url( '/contact/' ) ); ?>" class="btn rounded-0">Obtenez Votre Permis</a>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section id="section1" class="py-7">
    <div class="container">
      <div class="row">
        <div class="col-12 ">
          <p><?php _e('Connaissez-vous la location de voiture à double commande avec boite automatique avec 
            DK conduite? Découvrez nos nouveaux forfaits destinés à la location de voiture à double 
            commande en boîte automatique ! Contactez le bureau. Celle-ci a pour avantage de vous 
            proposer une nouvelle manière d’apprendre la conduite sans passer par un moniteur 
            d’auto-école. Autrefois inaccessible pour les foyers les plus modestes, c’est aujourd’hui 
            une solution démocratisée qui permet à tous d’apprendre à conduire avec confiance pour obtenir 
            son permis en un temps record pour les plus motivés. Avec une voiture à boite automatique, vous 
            apprenez comment manier un véhicule à pédales sans stress, pour passer rapidement l’examen final.', 'dkconduite'); ?>
          </p>
        </div>
      </div>
    </div>
  </section>

  <section id="section2" class="py-7">
    <div class="container">
      <div class="row">
        <div class="col-lg-6">
          <img src="<?php echo get_template_directory_uri(); ?>/images/DKconduite-4-1.png" class="img-fluid" alt="DKconduite">
        </div>
        <div class="col-lg-6">
          <h2 class="section-title"><?php _e('Les avantages de louer une voiture à double commande avec boite automatique</h2>
          <p>Si vous hésitez encore à sauter le pas, sachez que de très nombreuses agences ne proposent pas la voiture 
            à double commande avec boite automatique. C’est une solution pour tous ceux sont stressés par la commande 
            manuelle, ou qui savent s’ils en auront l’utilité ou non de par la suite. C’est aussi pratique pour ceux 
            qui doivent passer leur permis rapidement et qui envisagent une voiture avec une boite automatique dès 
            l’obtention du permis.', 'dkconduite'); ?>
          </p>
          <h4 class="sub-title"><?php _e('Se donner le temps de réussir', 'dkconduite'); ?></h4>
          <p>
            <?php _e('Le grand gagnant lors de la voiture à double commande avec boite automatique, c’est bel et bien le loueur. 
            En effet, il peut désormais se concentrer sur sa conduite et prévoir son planning toute la semaine, le samedi 
            et le dimanche en entier s’il le désire, et même lors des périodes scolaires.', 'dkconduite'); ?> 
          </p>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-6">
          <h4 class="sub-title"><?php _e('Le choix de son accompagnateur : une stratégie pour progresser vite', 'dkconduite'); ?></h4>
          <p>
            <?php _e('Mais ces longues heures de route ne sont pas parcourues seules. Lors de votre location de voiture à double commande 
            avec boite automatique, vous avez pour obligation de désigner une personne en tant qu’accompagnateur. Afin de faciliter 
            le choix de votre accompagnateur, celui qui va vous enseigner les dernières clés du code de la route et du maniement du 
            véhicule, il est préférable de choisir un membre proche et disponible sur vos créneaux horaires.', 'dkconduite'); ?>
          </p>
        </div>
        <div class="col-lg-6">
          <p>
            <?php _e('Notre conseil ? Que votre accompagnant soit une personne en qui vous avez confiance, avec qui vous pouvez dialoguer et 
            qui possède de la pédagogie. En tant qu’accompagnateur, son rôle est capital, sa désignation ne doit pas être faite à la légère.', 'dkconduite'); ?>
          </p>
        </div>
      </div>
    </div>
  </section>

  <section id="price-table" class="py-7">
    <div class="container">
      <div class="row justify-content-center">
        <h2 class="section-title text-center mb-7">NOS FORFAITS CLASSIQUE BOITE AUTOMATIQUE</h2>
        <div class="col-lg-3 col-md-6 mb-3 mx-auto">
          <div class="pricing-plan text-center h-100">
            <div class="pricing-title">
              <h3><?php _e('À L\'UNITÉ 1H', 'dkconduite'); ?></h3>
              <span>1H</span>
            </div>
            <div class="price">
              <span>€</span> 24
            </div>
            <div class="pricing-content clearfix">
              <ul>
                <li><?php _e('Valable 1 mois', 'dkconduite'); ?></li>
                <li><?php _e('Kilométrage illimité', 'dkconduite'); ?></li>
                <li><?php _e('Assurance incluse', 'dkconduite'); ?></li>
                <li><?php _e('Essence incluse', 'dkconduite'); ?></li>
              </ul>
            </div>
            <div class="price-btn">
              <a href="<?php echo esc_url( home_url( '/contact/' ) ); ?>">Louer</a>
            </div>
          </div>
        </div> 
        <div class="col-lg-3 col-md-6 mb-3 mx-auto">
          <div class="pricing-plan text-center h-100">
            <div class="pricing-title">
              <h3><?php _e('OPTION 5H', 'dkconduite'); ?></h3>
              <span>5H (24 €/H)</span>
            </div>
            <div class="price">
              <span>€</span>119
            </div>
            <div class="pricing-content clearfix">
              <ul>
                <li><?php _e('Valable 1 mois', 'dkconduite'); ?></li>
                <li><?php _e('Kilométrage illimité', 'dkconduite'); ?></li>
                <li><?php _e('Assurance incluse', 'dkconduite'); ?></li>
                <li><?php _e('Essence incluse', 'dkconduite'); ?></li>
              </ul>
            </div>
            <div class="price-btn">
              <a href="<?php echo esc_url( home_url( '/contact/' ) ); ?>">Louer</a>
            </div>
          </div>
        </div> 
        <div class="col-lg-3 col-md-6 mb-3 mx-auto">
          <div class="pricing-plan text-center h-100">
            <div class="pricing-title">
              <h3><?php _e('OPTION 10H', 'dkconduite'); ?></h3>
              <span>10H (22,50 €/H)</span>
            </div>
            <div class="price">
              <span>€</span>225
            </div>
            <div class="pricing-content clearfix">
              <ul>
                <li><?php _e('Valable 3 mois', 'dkconduite'); ?></li>
                <li><?php _e('Kilométrage illimité', 'dkconduite'); ?></li>
                <li><?php _e('Assurance incluse', 'dkconduite'); ?></li>
                <li><?php _e('Essence incluse', 'dkconduite'); ?></li>
              </ul>
            </div>
            <div class="price-btn">
              <a href="<?php echo esc_url( home_url( '/contact/' ) ); ?>">Louer</a>
            </div>
          </div>
        </div> 
        <div class="col-lg-3 col-md-6 mb-3 mx-auto">
          <div class="pricing-plan text-center  h-100">
            <div class="pricing-title">
              <h3>OPTION 21H</h3>
              <span>20H (22 €/H)</span>
            </div>
            <div class="price">
              <span>€</span>440
            </div>
            <div class="pricing-content clearfix">
              <ul>
                <li><?php _e('<strong>1H OFFERTE !</strong>', 'dkconduite'); ?></li>
                <li><?php _e('Valable 6 mois', 'dkconduite'); ?></li>
                <li><?php _e('Kilométrage illimité', 'dkconduite'); ?></li>
                <li><?php _e('Assurance incluse', 'dkconduite'); ?></li>
                <li><?php _e('Essence incluse', 'dkconduite'); ?></li>
              </ul>
            </div>
            <div class="price-btn">
              <a href="<?php echo esc_url( home_url( '/contact/' ) ); ?>">Louer</a>
            </div>
          </div>
        </div> 
        <h2 class="section-title text-center mb-7 mt-7">NOS FORFAITS « SUR-MESURE » BOITE AUTOMATIQUE</h2>
        <div class="col-lg-3 col-md-6 mb-3">
          <div class="pricing-plan text-center">
            <div class="pricing-title">
              <h3><?php _e('CANDIDAT LIBRE', 'dkconduite'); ?></h3>
              <span>5H</span>
            </div>
            <div class="price">
              <span>€</span>120
            </div>
            <div class="pricing-content clearfix">
              <ul>
                <li><?php _e('Assurance pour l\'examen', 'dkconduite'); ?></li>
                <li><?php _e('Kilométrage illimité', 'dkconduite'); ?></li>
                <li><?php _e('Essence incluse', 'dkconduite'); ?></li>
              </ul>
            </div>
            <div class="price-btn">
              <a href="<?php echo esc_url( home_url( '/contact/' ) ); ?>">Louer</a>
            </div>
          </div>
        </div> 
        <div class="col-lg-3 col-md-6 mb-3">
          <div class="pricing-plan text-center h-100">
            <div class="pricing-title">
              <h3><?php _e('OPTION WEEK-END & JOUR FERIES', 'dkconduite'); ?></h3>
              <span>(10H)</span>
            </div>
            <div class="price">
              <span>€</span>219
            </div>
            <div class="pricing-content clearfix">
              <ul>
                <li><?php _e('Kilométrage illimité', 'dkconduite'); ?></li>
                <li><?php _e('Assurance & Essence incluse', 'dkconduite'); ?></li>
              </ul>
            </div>
            <div class="price-btn">
              <a href="<?php echo esc_url( home_url( '/contact/' ) ); ?>">Louer</a>
            </div>
          </div>
        </div>    
      </div>
    </div>
  </section>

  <section id="section2" class="py-7">
    <div class="container">
      <div class="row">
        <div class="col-lg-6">
          <img src="<?php echo get_template_directory_uri(); ?>/images/DKconduite-4-1.png" class="img-fluid" alt="DKconduite">
        </div>
        <div class="col-lg-6">
          <h2 class="section-title"><?php _e('Réalisez des économies sur le court terme</h2>
          <p>Il est très présent dans l’esprit collectif que la location d’une voiture à double commande avec boite automatique 
            est moins rentable que de passer son permis avec une auto-école classique. C’est pourtant le contraire ! En moyenne, 
            un cours de conduite coûte entre 50 et 65€. En comptant l’essence dépensée, les heures de pratiques jusqu’au passage 
            de l’examen, vous passez à 1 heure de cours entre 20 et 25€. C’est bel et bien la preuve que si vous êtes motivé à 
            passer le permis, vous avez toutes les chances de le réussir rapidement.', 'dkconduite'); ?>
          </p>
          <h4 class="sub-title"><?php _e('Les avantages de la boite automatique', 'dkconduite'); ?></h4>
          <p>
            <?php _e('À la différence de nombreuses agences, notre agence DK conduitevous permet de louer une voiture à double commande avec 
            boite automatique. La boite automatique repose sur un système de 2 pédales, l’accélérateur et le frein. À la différence 
            de la commande manuelle où vous devez écouter votre moteur, manier les pédales selon les 5 vitesses, avec la boite automatique 
            vous n’avez plus que deux pédales à gérer et non plus trois.', 'dkconduite'); ?> 
          </p>
          <p>
            <?php _e('C’est un avantage considérable pour tous les élèves qui savent déjà qu’ils comptent conduire une voiture automatique dès l’obtention 
            de leur permis. C’est aussi un moyen de gagner en confiance sur la route, afin d’être plus réactif sur la route. Bien que la voiture à 
            double commande avec boite automatique soit moins populaire en France que la voiture à commande manuelle, elle est très prisée en Amérique 
            ou en Asie, vous assurant de pouvoir conduire même à l’étranger.', 'dkconduite'); ?>
          </p>
        </div>
      </div>
    </div>
  </section>

  <section id="banner" class="banner py-7" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/DK-banner.jpg');">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="call-to-action text-center">
            <div class="area-title">
              <h6><?php _e('// DKConduite : Nos voitures à double pédale //', 'dkconduite'); ?></h6>
              <h2><?php _e('Pour apprendre à votre rythme, sans stress ni pression, optez pour la location de voiture à double commande avec DKConduite.', 'dkconduite'); ?></h2>
            </div>
            <div class="btn-wrapper">
              <a href="<?php echo esc_url( home_url( '/contact/' ) ); ?>" class="btn-wrap-2">Nous Contacter</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="dk-to-left"><img src="<?php echo get_template_directory_uri(); ?>/images/DKconduite-4-1.png" class="img-fluid" alt=""></div>
    <div class="dk-to-right"><img src="<?php echo get_template_directory_uri(); ?>/images/Dkuser.png" class="img-fluid" alt=""></div>
  </section>

  
<?php get_footer(); ?>