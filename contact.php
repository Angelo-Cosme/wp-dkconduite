<?php
/**
 * Template name: Contact
 *
 * @package WordPress
 * @since DKConduite 0.1
 */

get_header(); ?>

  <section id="slide" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/full-section-img.jpg');">
    <div class="container">
      <div class="row">
        <div class="col-lg-6">
          <div class="img-frame">
            <figure><img src="<?php echo get_template_directory_uri(); ?>/images/bg-slide.png" class="img-fluid" alt="permis"></figure>
          </div>
        </div>
        <div class="col-lg-6">
          <div class="text-slide">
            <h1>Louez, apprenez et conduisez</h1>
            <p>DKConduite : votre location de voiture à double commande pour un permis B moins cher !</p>
            <a href="<?php echo esc_url( home_url( '/pour-qui/' ) ); ?>" class="btn">Obtenez Votre Permis</a>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section id="contact" class="contact py-11">
    <div class="container">
      <div class="row">
      <div class="col-lg-9 mb-5">
          <div class="row">
            <div class="col-lg-12">
              <div class="form-group mb-3">
                <label for="objet">Objet <span class="text-danger">*</span></label>
                <input type="text" name="your-subject" size="50" class="form-control w-100" aria-required="true" aria-invalid="false">
              </div>
            </div>
            <div class="col-lg-6">
              <div class="form-group mb-3">
                <label for="name">Nom <span class="text-danger">*</span></label>
                <input type="text" name="your-name" size="50" class="form-control w-100" aria-required="true" aria-invalid="false">
              </div>
            </div>
            <div class="col-lg-6">
              <div class="form-group mb-3">
                <label for="name-second">Prénom <span class="text-danger">*</span></label>
                <input type="text" name="your-name" size="50" class="form-control w-100" aria-required="true" aria-invalid="false">
              </div>
            </div>
            <div class="col-lg-12">
              <div class="form-group mb-3">
                <label for="phone">Téléphone <span class="text-danger">*</span></label>
                <input type="text" name="your-subject" size="50" class="form-control w-100" aria-required="true" aria-invalid="false">
              </div>
            </div>
            <div class="col-lg-12">
              <div class="form-group mb-3">
                <label for="message">Message <span class="text-danger">*</span></label>
                <textarea name="message" class="form-control w-100" id="" cols="30" rows="10"></textarea>
              </div>
            </div>
          </div>
          <p><input type="submit" class="form-control btn btn-primary btn-send" value="Envoyer"></p>
        </div>

        <div class="col-lg-3 mb-5">
          <h3 class="contact-form-title">Contactez-nous</h3>
          <div class="row">
            <div class="col-lg-12">
              <div class="contact-item">
                <img class="mr-3" src="<?php echo get_template_directory_uri(); ?>/images/icon-address.png" alt="DKConduite adresse">
                <span>4800, Lorem, ipsum dolor sit amet consectetur adipisicing.</span>
              </div>
            </div>
            <div class="col-lg-12">
              <div class="contact-item">
                <img class="mr-3" src="<?php echo get_template_directory_uri(); ?>/images/icon-phone.png" alt="DKConduite adresse">
                <a href="tel:1 0123456789632">+1 0123456789632</a>
              </div>
            </div>
            <div class="col-lg-12">
              <div class="contact-item">
                <img class="mr-3" src="<?php echo get_template_directory_uri(); ?>/images/icon-email.png" alt="DKConduite adresse">
                <span>contact@dkconduite.com</span>
              </div>
            </div>
            <h3 class="contact-form-title mb-4">Louez une voiture</h3>
            <a href="<?php echo esc_url( home_url( '/louer/' ) ); ?>" class="btn btn-primary">Je loue un véhicule</a>
          </div>
        </div>
      </div>
    </div>
  </section>

<?php get_footer(); ?>